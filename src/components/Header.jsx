import React from 'react';

function Header() {
    return <div className="header-container">
    <div className="header-nav">
    <div className="menu-container">

        <ul className="menu-list">
             <li className="menu-item">
                 <a href="#" class="menu-link">Reactions</a>
            </li>

            <li className="menu-item">
                <a href="#" class="menu-link">Entertainment</a>
            </li>
            <li className="menu-item">
                <a href="#" class="menu-link">Sports</a>
            </li>
            <li className="menu-item">
                <a href="#" class="menu-link">Stickers</a>
            </li>
            <li className="menu-item">
                <a href="#" class="menu-link">Artists</a>
            </li>
        </ul>
        </div>
        <div className="icono-header">
            <a className="icono-create" rel="nofollow" href="/upload">
                <span className="ico-upload-create1">Upload</span>
            </a>
                <a className="icono-create2" rel="nofollow" href="/create/gifmaker">
                    <span className="ico-upload-create2">Create</span>
                </a>
                    </div>
                    <div className="login-container">
                        <a className="login-button" rel="nofollow" href="/login">
                        <i className="ss-user"></i><span className="nav-text">Log In</span>
                            </a>
                            </div>
        </div>
        </div>
}

export default Header;