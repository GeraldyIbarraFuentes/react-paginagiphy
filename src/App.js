import React from 'react';
import logo from './logo.svg';
import './App.css';
import Input from './components/Input';
import Header from './components/Header';
import Slider from './components/Slider';


const paragraph = 'Este es un ejemplo de JSX. Un parrafo muy largo.';
function App() {
  return (
    <div className="App">
      <header className="App-header">
      <Header />
      
      <Input />
        <Slider />
        
      </header>
    </div>
  );
}

export default App;
